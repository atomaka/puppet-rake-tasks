# Puppet Rake Tasks

## Description
A set of rake tasks useful while working with puppet modules.

## Usage

```code
rake validate:pp
```

Runs 'puppet parser validate' on all files in your manifests directory.

```code
rake validate:lint
```

Runs 'puppet-lint' on all files in your manifests directory.

```code
rake validate:erb
```

Validates erb syntax on all files in your templates directory.
